# Designing in QML

## Table of Contents

- [Requirements](#requirements)
- [Setup](#setup)
- [About](#about)
- [Maintainers](#maintainers)
- [Contributers](#contributers)

## Requirements

Requires `Qt Creator 6.2.3`.

## Setup
* Build and Run through QT Creator

## About
Example of design in QML.

## Maintainers

[Thorbjørn (@Thunderl3ear)](https://gitlab.com/Thunderl3ear)

## Contributers and License
Copyright 2022,

[Thorbjørn (@Thunderl3ear)](https://gitlab.com/Thunderl3ear)

