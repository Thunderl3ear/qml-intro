import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls
import QtQuick.Layouts

Window {
    width: 400
    height: 550
    visible: true
    title: qsTr("Quiz Game")
    color: "#1B2631"
    ColumnLayout {
        spacing: 40
        height: parent.height
        width: parent.width
        Layout.preferredWidth: parent.width
        Label {
            width: parent.width
            text: "Quiz Game"
            font.pointSize: 36
            Layout.alignment: Qt.AlignCenter
        }
        GridLayout{
            columns: 2
            width: parent.width
            Layout.preferredWidth: parent.width
            ColumnLayout{
                Layout.alignment: Qt.AlignCenter
                Label {
                    text: "Time left"
                    font.pointSize: 16
                    Layout.alignment: Qt.AlignCenter
                }
                Label {
                    text: "13"
                    font.pointSize: 48
                    font.italic: true
                    Layout.alignment: Qt.AlignCenter
                }
            }
            GridLayout{
                columns: 2
                Layout.alignment: Qt.AlignCenter
                Label {
                    text: "User"
                    font.pointSize: 16
                    Layout.alignment: Qt.AlignLeft
                }
                Label {
                    text: "user_id"
                    font.pointSize: 16
                    font.italic: true
                    Layout.alignment: Qt.AlignRight
                }
                Label {
                    text: "Score"
                    font.pointSize: 16
                    Layout.alignment: Qt.AlignLeft
                }
                Label {
                    text: "points"
                    font.pointSize: 16
                    font.italic: true
                    Layout.alignment: Qt.AlignRight
                }
                Label {
                    text: "Level"
                    font.pointSize: 16
                    Layout.alignment: Qt.AlignLeft
                }
                Label {
                    text: "level"
                    font.pointSize: 16
                    font.italic: true
                    Layout.alignment: Qt.AlignRight
                }
                Label {
                    text: "Streak"
                    font.pointSize: 16
                    Layout.alignment: Qt.AlignLeft
                }
                Label {
                    text: "streak"
                    font.pointSize: 16
                    font.italic: true
                    Layout.alignment: Qt.AlignRight
                }
                Label {
                    text: "Speed"
                    font.pointSize: 16
                    Layout.alignment: Qt.AlignLeft
                }
                Label {
                    text: "speed"
                    font.pointSize: 16
                    font.italic: true
                    Layout.alignment: Qt.AlignRight
                }
            }
        }
        Label {
            text: "Quiz question"
            wrapMode: Label.Wrap
            font.pointSize: 16
            font.italic: true
            Layout.alignment: Qt.AlignCenter
        }
        GridLayout {
            columnSpacing: 40
            rowSpacing: 40
            Layout.fillWidth: true
            Layout.alignment: Qt.AlignCenter
            columns: 2
            Button {
                text: "One"
                font.italic: true
                background: Rectangle {
                    implicitWidth: 100
                    implicitHeight: 40
                    border.color: "#3498DB"
                    border.width: 3
                    radius: 4
                    //color: "#1B2631"
                    color: parent.down ? "#3498DB" : (parent.hovered ? "#3498DB" : "#1B2631")
                }
            }
            Button {
                text: "Two"
                font.italic: true
                background: Rectangle {
                    implicitWidth: 100
                    implicitHeight: 40
                    border.color: "#3498DB"
                    border.width: 3
                    radius: 4
                    color: "#1B2631"
                }
            }
            Button {
                text: "Three"
                font.italic: true
                background: Rectangle {
                    implicitWidth: 100
                    implicitHeight: 40
                    border.color: "#3498DB"
                    border.width: 3
                    radius: 4
                    color: "#1B2631"
                }
            }
            Button {
                text: "Four"
                font.italic: true
                background: Rectangle {
                    implicitWidth: 100
                    implicitHeight: 40
                    border.color: "#3498DB"
                    border.width: 3
                    radius: 4
                    color: "#1B2631"
                }
            }
        }
        ProgressBar {
            id: control
            value: 0.35
            padding: 4

            background: Rectangle {
                implicitWidth: parent.parent.width
                implicitHeight: 40
                color: "#1B2631"
                radius: 4
                border.color: "#48C9B0"
                border.width: 3
            }
            contentItem: Item {
                implicitWidth: parent.parent.width-8
                implicitHeight: 25

                Rectangle {
                    width: control.visualPosition * parent.width
                    height: parent.height
                    radius: 0
                    color: "#48C9B0"
                }
            }
            Layout.alignment: Qt.AlignBottom
        }
    }
}
