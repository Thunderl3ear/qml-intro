#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QFont>
#include <QPalette>

int main(int argc, char *argv[])
{
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif
    QGuiApplication app(argc, argv);

    QPalette pal= QPalette();
    pal.setColor(QPalette::ButtonText, QColor("#3498DB"));
    pal.setColor(QPalette::WindowText, QColor("#48C9B0"));
    app.setPalette(pal);

    app.setFont(QFont("mononoki", 18));

    QQmlApplicationEngine engine;
    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);

    return app.exec();
}
